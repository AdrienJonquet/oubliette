extern crate ggez;
extern crate rand;

use ggez::conf;
use ggez::event::*;
use ggez::graphics;
use ggez::timer;
use ggez::{Context, ContextBuilder, GameResult};

use rand::Rng;
use std::env;
use std::path;
use std::time;
use std::time::Duration;

const SPEED: f32 = 10.0;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum LinkState {
    Stopped,
    Moving,
}

impl Direction {
    /// We create a helper function that will allow us to easily get the inverse
    /// of a `Direction` which we can use later to check if the player should be
    /// able to move the snake in a certain direction.
    pub fn inverse(&self) -> Self {
        match *self {
            Direction::Up => Direction::Down,
            Direction::Down => Direction::Up,
            Direction::Left => Direction::Right,
            Direction::Right => Direction::Left,
        }
    }

    /// We also create a helper function that will let us convert between a
    /// `ggez` `Keycode` and the `Direction` that it represents. Of course,
    /// not every keycode represents a direction, so we return `None` if this
    /// is the case.
    pub fn from_keycode(key: Keycode) -> Option<Direction> {
        match key {
            Keycode::Up => Some(Direction::Up),
            Keycode::Down => Some(Direction::Down),
            Keycode::Left => Some(Direction::Left),
            Keycode::Right => Some(Direction::Right),
            _ => None,
        }
    }
}

struct Link {
    dir: Direction,
    state: LinkState,
    x_pos: f32,
    y_pos: f32,
    walk_down_1: graphics::spritebatch::SpriteBatch,
    walk_down_2: graphics::spritebatch::SpriteBatch,
}

impl Link {
    fn new(_ctx: &mut Context) -> GameResult<Link> {
        let link_down_1 = graphics::Image::new(_ctx, "/link_down_1.png")?;
        let link_down_2 = graphics::Image::new(_ctx, "/link_down_2.png")?;
        let link_down_1_sb = graphics::spritebatch::SpriteBatch::new(link_down_1.clone());
        let link_down_2_sb = graphics::spritebatch::SpriteBatch::new(link_down_2.clone());
        Ok(Link {
            dir: Direction::Down,
            state: LinkState::Stopped,
            x_pos: 0.0,
            y_pos: 0.0,
            walk_down_1: link_down_1_sb,
            walk_down_2: link_down_2_sb,
        })
    }

    fn update(&mut self) {}

    fn draw(&self, _ctx: &mut Context) -> GameResult<()> {
        Ok(())
    }
}

struct MainState {
    link: Link,
    wind_fish: graphics::spritebatch::SpriteBatch,
    frames: usize,
    offset: f32,
    last_start: time::Duration,
}

impl MainState {
    fn new(mut _ctx: &mut Context) -> GameResult<MainState> {
        let wind_fish = graphics::Image::new(_ctx, "/wind_fish.png")?;
        let wind_fish_sb = graphics::spritebatch::SpriteBatch::new(wind_fish.clone());
        Ok(MainState {
            link: Link::new(&mut _ctx)?,
            wind_fish: wind_fish_sb,
            frames: 0,
            offset: 0.0,
            last_start: time::Duration::from_secs(0),
        })
    }
}

impl EventHandler for MainState {
    fn key_down_event(
        &mut self,
        _ctx: &mut Context,
        keycode: Keycode,
        _keymod: Mod,
        _repeat: bool,
    ) {
        match keycode {
            Keycode::Down => {
                self.link.dir = Direction::Down;
                self.link.state = LinkState::Moving;
            }
            _ => (),
        }
    }

    fn key_up_event(&mut self, _ctx: &mut Context, keycode: Keycode, _keymod: Mod, _repeat: bool) {
        match keycode {
            Keycode::Down => {
                self.link.state = LinkState::Stopped;
            }
            _ => (),
        }
    }

    fn update(&mut self, _ctx: &mut Context) -> GameResult<()> {
        if self.link.state == LinkState::Moving {
            if self.link.dir == Direction::Down {
                if self.link.y_pos < 470.0 {
                    self.link.y_pos +=
                        5.0 * (ggez::timer::get_delta(_ctx).subsec_nanos() as f32 / 1e8);
                }
            }
        }
        Ok(())
    }

    fn draw(&mut self, _ctx: &mut Context) -> GameResult<()> {
        graphics::clear(_ctx);
        let scale = 2.0;
        let scale_point = graphics::Point2::new(scale, scale);

        let p = graphics::DrawParam {
            dest: graphics::Point2::new(self.link.x_pos, self.link.y_pos),
            scale: scale_point,
            rotation: 0.0,
            ..Default::default()
        };

        if self.link.state == LinkState::Moving {
            if self.link.dir == Direction::Down {
                if self.frames % 30 < 15 {
                    self.link.walk_down_1.add(p);
                    graphics::draw_ex(_ctx, &self.link.walk_down_1, p)?;
                    self.link.walk_down_1.clear();
                } else {
                    self.link.walk_down_2.add(p);
                    graphics::draw_ex(_ctx, &self.link.walk_down_2, p)?;
                    self.link.walk_down_2.clear();
                }
            }
        } else if self.link.state == LinkState::Stopped {
            self.link.walk_down_1.add(p);
            graphics::draw_ex(_ctx, &self.link.walk_down_1, p)?;
            self.link.walk_down_1.clear();
        }

        graphics::present(_ctx);
        self.frames += 1;
        if (self.frames % 100) == 0 {
            println!("FPS: {}", ggez::timer::get_fps(_ctx));
        }
        Ok(())
    }
}

pub fn main() {
    let mut cb = ContextBuilder::new("Oubliette", "tamago")
        .window_setup(conf::WindowSetup::default().title("Oubliette"))
        .window_mode(conf::WindowMode::default().dimensions(1280, 720));

    // We add the CARGO_MANIFEST_DIR/resources to the filesystems paths so
    // we we look in the cargo project for files.
    if let Ok(manifest_dir) = env::var("CARGO_MANIFEST_DIR") {
        let mut path = path::PathBuf::from(manifest_dir);
        path.push("resources");
        println!("Adding path {:?}", path);
        // We need this re-assignment alas, see
        // https://aturon.github.io/ownership/builders.html
        // under "Consuming builders"
        cb = cb.add_resource_path(path);
    } else {
        println!("Not building from cargo?  Ok.");
    }

    let ctx = &mut cb.build().unwrap();
    graphics::set_default_filter(ctx, graphics::FilterMode::Nearest);

    match MainState::new(ctx) {
        Err(e) => {
            println!("Could not load game!");
            println!("Error: {}", e);
        }
        Ok(ref mut game) => {
            let result = run(ctx, game);
            if let Err(e) = result {
                println!("Error encountered running game: {}", e);
            } else {
                println!("Game exited cleanly.");
            }
        }
    }
}
